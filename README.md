# GitLab CI Demo Application

> A demo application for the `DevOps and Cloud Computing` Course at the UAS Technikum Wien

## Prerequesits

* [Node version 14.x](https://nodejs.org/en/)

## Usage

Install all dependencies

```sh
$ npm i
```

Start the application for development

```sh
$ npm start
```

Lint the code

```sh
$ npm run lint
```

Run all tests

```sh
$ npm test
```

Build the application

```sh
$ npm run build
```

Clean application cache, build and clean and reinstall node_modules

```sh
$ npm run cleanup
```
